{
  name: "example-elixir-app",
  labels: {
    app: $.name,
  },
  namespace: "default",
  replicas: 1,
  volumes: {},
  image_repo: "localhost:32000/entropealabs",
  image_name: $.name,
  image_tag: "latest",
  args: ["start"],
  env_vars: {
    TEST: "example"
  },
  cpu_request: "100m",
  cpu_limit: "150m",
  memory_request: "150Mi",
  memory_limit: "200Mi",
  ports: {
    http: {
      containerPort: 4000, 
      protocol: "TCP"
    }
  },
  readiness_probe: {
    httpGet: {path: '/', port: 4000}
  },
  liveness_probe: {
    httpGet: {path: '/', port: 4000}
  },
  volumeMounts: {}
}
