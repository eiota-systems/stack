local app = import '../lib/app.jsonnet';
local default = import 'default-config.jsonnet';

app + {
  config: default + {
    name: "router",
    replicas: 3,
    image_path: "localhost:32000/entropealabs/router",
    env_vars: {
      SEED_TENANT: "org.entropealabs",
      SEED_REALM: "admin",
      SEED_AUTHID: "admin",
      SEED_PASSWORD: "test1234",
      PLATFORM_PASSWORD: "test1234",
      AUTH_DATABASE_NAME: "auth",
      AUTH_DATABASE_HOSTNAME: "cockroachdb",
      AUTH_DATABASE_USERNAME: "root"
    }
  }  
}
