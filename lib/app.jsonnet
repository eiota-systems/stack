local kube = import '../kube-libsonnet/kube.libsonnet';

{
  config:: error "you need to provide configuration values: see default-config.jsonnet",
  service: kube.Service($.config.name) {
    metadata+: {namespace: $.config.namespace},
    target_pod: $.deployment.spec.template
  },
  deployment: kube.Deployment($.config.name) {
    metadata+:{
      labels+: $.config.labels,
      namespace: $.config.namespace
    },
    spec+: {
      replicas: $.config.replicas,
      template+: {
        spec+: {
          volumes_+: $.config.volumes,
          containers_: {
            app: kube.Container($.config.name) {
              image: $.config.image_repo + '/' + $.config.image_name + ':' + $.config.image_tag,
              args: $.config.args,
              env_: $.config.env_vars,
              resources: {
                requests: {
                  cpu: $.config.cpu_request, 
                  memory: $.config.memory_request
                },
                limits: {
                  cpu: $.config.cpu_limit, 
                  memory: $.config.memory_limit
                }
              },
              ports_+: $.config.ports,
              readinessProbe: $.config.readiness_probe,
              livenessProbe: $.config.liveness_probe,
              volumeMounts_+: $.config.volumeMounts
            }
          }
        }
      }
    }
  }
}
