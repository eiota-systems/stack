local router = import '../common/router.jsonnet';

router + {
  config+: {
    namespace: "dev"
  }
}
