# Stack

Currently just a docker-compose file to spin up the EIOTA Systems stack, eventually all of the Kubernetes manifests for deploying the platform.

The docker-compose files assume you have checked out all the relevant repos into the parent directory of this checkout.


```bash
docker-compose \
  -f docker-compose.yml \
  -f docker-compose.router.yml \
  -f docker-compose.device-shadow.yml \
  -f docker-compose.telemetry-collector.yml \
  -f docker-compose.alert-manager.yml \
  -f docker-compose.admin-interface.yml \
  -f docker-compose.wampex-example-app.yml \
  -f docker-compose.scada.yml up
```
